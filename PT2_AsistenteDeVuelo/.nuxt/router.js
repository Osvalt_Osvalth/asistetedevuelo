import Vue from 'vue'
import Router from 'vue-router'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _6ebff4b4 = () => interopDefault(import('..\\pages\\hisvuelos.vue' /* webpackChunkName: "pages_hisvuelos" */))
const _282189a4 = () => interopDefault(import('..\\pages\\iniciarVuelo2.vue' /* webpackChunkName: "pages_iniciarVuelo2" */))
const _16d62b48 = () => interopDefault(import('..\\pages\\index.vue' /* webpackChunkName: "pages_index" */))

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: decodeURI('/'),
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
      path: "/hisvuelos",
      component: _6ebff4b4,
      name: "hisvuelos"
    }, {
      path: "/iniciarVuelo2",
      component: _282189a4,
      name: "iniciarVuelo2"
    }, {
      path: "/",
      component: _16d62b48,
      name: "index"
    }],

  fallback: false
}

export function createRouter() {
  return new Router(routerOptions)
}
