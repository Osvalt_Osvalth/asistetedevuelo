/*
**  Nuxt
*/

const http = require('http');
const { Nuxt, Builder } = require('nuxt');
const {download} = require('electron-dl');
let config = require('./nuxt.config.js');
config.rootDir = __dirname; // for electron-builder
// Init Nuxt.js
const nuxt = new Nuxt(config);
const builder = new Builder(nuxt);
const server = http.createServer(nuxt.render);

// Build only in dev mode
if (config.dev) {
	builder.build().catch(err => {
		console.error(err); // eslint-disable-line no-console
		process.exit(1)
	})
}
// Listen the server
server.listen()
const _NUXT_URL_ = `http://localhost:${server.address().port}`;
console.log(`Nuxt working on ${_NUXT_URL_}`);

/*
** Electron
*/
let win = null; // Current window
const electron = require('electron');
const path = require('path');
const app = electron.app;
const newWin = () => {
	win = new electron.BrowserWindow({
		icon: path.join(__dirname, 'static/potro.png'),
		webPreferences: {webSecurity: false},
		width: 600,
		height: 800
	});
	win.maximize()
	win.on('closed', () => win = null);
	if (config.dev) {
		// Install vue dev tool and open chrome dev tools
		const { default: installExtension, VUEJS_DEVTOOLS } = require('electron-devtools-installer');
		installExtension(VUEJS_DEVTOOLS.id).then(name => {
			console.log(`Added Extension:  ${name}`);
			win.webContents.openDevTools()
		}).catch(err => console.log('An error occurred: ', err));
		// Wait for nuxt to build
		const pollServer = () => {
			http.get(_NUXT_URL_, (res) => {
				if (res.statusCode === 200) { win.loadURL(_NUXT_URL_) } else { setTimeout(pollServer, 300) }
			}).on('error', pollServer)
		};
		pollServer()
	} else { return win.loadURL(_NUXT_URL_) }
	electron.ipcMain.on('download', (event, info)=>{
		download(electron.BrowserWindow.getFocusedWindow(), info.url, info.properties)
			.then(dl => win.webContents.send('download complete', dl.getSavePath()))
	})
	/**
	 * https://stackoverflow.com/questions/53778405/electron-js-access-post-data-from-loadurl-method
	 * https://stackoverflow.com/questions/54640224/how-to-send-data-from-one-electron-window-to-another-with-ipcrenderer-using-vuej
	 * https://stackoverflow.com/questions/43646844/cannot-read-property-post-of-undefined-vue
	 * Revisar con la cámara
	 */
	electron.ipcMain.on('upload', (event, datosRec)=> {
		//console.log(JSON.stringify(datosRec));  console.log('1 ->' +datosRec.datos.url)
		//console.log('2 ->' + Object.keys(datosRec.datos.solapamiento))
		//console.log('3 ->' + Object.values(datosRec.datos.url))
		win.loadURL(datosRec.datos.url + 'post', {
			postData: [{
				type: 'rawData',
				//bytes: Buffer.from(datosRec.arguments)
				bytes: Buffer.from('{"timelapse_param":'+ datosRec.datos.solapamiento +'}')
			}],
			extraHeaders: 'Content-Type: application/x-www-form-urlencoded'
		})/*.then(event.sender.send('upload complete', true))
			.catch(event.sender.send('upload complete', false))*/

	})

	/*
	ipcMain.on("checkPerl", function(e){
		tryToRun("perl", ["-v"])
			.then(function(){ e.sender.send("checkPerlReply", true) })
			.catch(function(){ e.sender.send("checkPerlReply", false) })
	})*/
};
app.on('ready', newWin);
app.on('window-all-closed', () => app.quit());
app.on('activate', () => win === null && newWin());

